####Progetto finale GBD - Riccardo Cimboli & Gabriele Fabozzi #######

Salve professore,

in questa cartella vi � tutto il lavoro da noi svolto. Vi trover� i seguenti file

1) listaSpecie.csv: un file .csv con la lista delle 19405 specie che abbiamo considerato.

2) lista_sequenze.txt: file di testo contenente le sequenze delle specie considerate.

3) lista_mini.txt: file di testo contenente una versione ridotta del dataset.

4) Documento riassuntivo.pdf: un documento riassuntivo del nostro lavoro

5) Una cartella, Codice, in cui vi � tutto il codice Java per implementare l'applicazione. All'interno di essa, vi � la classe 
contenente il metodo main, HCA.java. In questa cartella pu� specificare la cartella di input dove trovare i file lista_sequenze.txt o 
lista_mini.txt modificando il campo "input". 

Inoltre, alla fine della classe vi � la parte di collegamento con Neo4j che pu� tranquillamente disattivare ponendo il campo
debug = false.

A presto,

RC
GF