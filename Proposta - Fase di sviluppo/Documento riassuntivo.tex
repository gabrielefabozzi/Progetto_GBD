\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[left=2.5cm,top=3cm,right=2.5cm,bottom=3cm]{geometry} %Setting margins
\usepackage{parskip} %disables automatic indentation
\usepackage{graphicx}%graphics package
\usepackage{hyperref}%permette l'inserimento di collegamente ipertestuali
\usepackage{amsmath}%display di formule matematiche
\usepackage{algorithm,algorithmic}%per incapsulare l'algoritmo

\title{{Algoritmo di clustering gerarchico distribuito}}

\author{Riccardo Cimboli \& Gabriele Fabozzi}
\date{Gestione di Big Data - Luglio 2018}

\begin{document}

\maketitle

\section{Problema e strategia di risoluzione}

La sfida di tale progetto risiedeva nell'effettuare clustering gerarchico su una collezione di sequenze di RNA ribosomiali del tipo 16S di un gruppo di 19405 specie di batteri. In particolare, si voleva costruire il dendrogramma tassonomico di tale collezione di specie. Il metodo di clustering gerarchico (agglomerativo) è un metodo di costruzione di cluster che prevede il progressivo accorpamento di elementi a cluster preesistenti o a cluster nuovi. 

\subsection{Recupero dati, allineamento sequenze, lettura}

I dati sono stati recuperati dall'archivio NCBI, in particolare dal sottoarchivio \href{https://www.ncbi.nlm.nih.gov/nuccore?term=33175\%5BBioProject\%5D+OR+33317\%5BBioProject\%5D}{Bacterial 16S Ribosomal RNA RefSeq Targeted Loci Project}. Essi sono stati recuperati tramite il gestore GNU Wget. Le sequenze sono state scaricate in formato FASTA, sostanzialmente file di testo in cui ogni coppia di righe contiene, nella prima riga, informazioni sulla sequenza, e nella seconda le sequenze vere e proprie. Tali sequenze sono di lunghezze diverse (tra le 1300 e 1500 basi), e ciò è stato un grosso ostacolo in fase di preparazione. 

Il problema risiedeva nel fatto che per rendere le sequenze confrontabili (ovvero per poter calcolare delle distanze fra sequenze) esse avrebbero dovuto essere della stessa lunghezza. Per affrontare tale problema si è dovuto ricorrere ad un'operazione che nel campo della genetica è detta allineamento. L'allineamento fra una coppia di sequenze genomiche consiste nell'inserire all'interno delle sequenze, ove opportuno, degli spazi (detti \textit{gaps}) in modo da massimizzare il numero di posizioni in cui vi è un combaciamento (detto \textit{match}) tra le basi delle due sequenze una volta che quest'ultime sono state messe una accanto all'altra. Il vincolo dell'allineamento sta nel fatto che le lunghezze delle sequenze con i \textit{gaps} devono essere identiche. Il concetto si può poi estendere ad $N$ sequenze, e si ha quindi a che fare con un problema di programmazione lineare non banale. 

Tali allineamenti vengono eseguiti con software appositi, e quello utilizzato in questo lavoro è stato \href{http://eddylab.org/infernal/}{Infernal}. In particolare, si è utilizzato un pacchetto di Infernal chiamato \href{http://eddylab.org/software/ssu-align/}{ssu-align} e tarato per avere a che fare con sequenze di RNA ribosomiale di questo tipo. Il suo limite è che gira esclusivamente su piattaforme \textit{POSIX-compatible}, mentre il suo principale vantaggio è che al suo interno è previsto un algoritmo di allineamento delle sequenze distribuito. L'allineamento ha prodotto un gruppo di 19405 sequenze allineate di lunghezza 6746, inclusiva di basi e \textit{gap}. Il formato dell'output con il quale si è deciso di lavorare è Stockholm (estensione .stk), dove ogni coppia di righe è composto da una prima riga con nome della sequenza e la sequenza allineata, ed una seconda riga con informazioni aggiuntive relative al processo di allineamento. 

Per quanto riguarda la fase di caricamento dei dati, si è dovuto tenere conto della struttura dei file di output di Infernal, e quindi i metodi per la lettura dei dati sono stati costruiti all'uopo. 

\subsection{Definizione della metrica e del \textit{linkage criterion}}

Innazitutto è stato necessario definire una metrica (che definirà la distanza fra coppie di elementi) ed un criterio di collegamento (che definirà la distanza fra coppie di cluster).

Tale metrica doveva essere in grado di fornire una qualche misura di distanza fra sequenze genomiche, che non sono altro che stringhe di caratteri (dove ogni base ha una determinata posizione), possibilmente coerente con la natura del problema. Dopo una breve rassegna della letteratura in materia, si è optato per una distanza un po' \textit{sui generis} ispirata alla distanza di Hamming. Essa confronta coppie di sequenze andando a vedere posizione per posizione se vi è una corrispondenza fra le basi nucleotidiche, associando un punteggio di conseguenza. La distanza fra sequenze sarà naturalmente più bassa più le sequenze sono simili. Purtroppo, le sequenze allineate in output da Infernal non prevedono solo la presenza di basi, ma anche di \textit{gap}. I \textit{gap} possono essere di due tipi diversi: i \textit{gap} marcati come trattini (-) e quelli marcati come punti (.). In breve, per una determinata sequenza, il trattino in una certa posizione indica un \textit{gap} dove di solito (in altre sequenze) si ha invece una base. Il puntino invece rapprenseta quei \textit{gap} che sono \textit{gap} nella maggior parte delle sequenze. Basandosi sulla documentazione di Infernal, si è costruita quindi la seguente tabella di punteggi per il confronto tra due sequenze ed il calcolo della distanza fra le due. Si prende come riferimento una generica base ``A'', ed il carattere speciale (*) indica una base oppure un \textit{gap}. 

\begin{center}
	\begin{tabular}{ |c|c|c|c|c|c| } 
		 \hline
		 Combinazione & AA & AG & .. & -* & .A  \\ 
		 \hline
 		Punteggio & 0 & 2 & 0 & 1 & 2 \\ 
 		 \hline
	\end{tabular}
\end{center}

Come criterio di collegamento si è scelto invece quello della distanza minima (dove la distanza è sempre definita secondo lo schema personalizzato di cui sopra), ovvero il cosiddetto \textit{single-linkage criterion}.

Una soluzione tradizionale al problema per la costruzione del dendrogramma è quella di calcolare l'albero minimo ricoprente (minimum spanning tree - MST) di un grafo completo costituito da tutti i punti. Per calcolare i MST di grafi completi si ricorre sovente ad algoritmi di tipo Kruskal o Prim. La soluzione che quindi si è voluta adottare è quella di un algoritmo di Prim distribuito.

\subsection{Strategia di risoluzione}

Il problema purtroppo si presta abbastanza male ad essere parallelizzato, in quanto le dipendenze fra elementi del grafo sono fittissime. Si è quindi cercata una soluzione ibrida che permettesse di dividere il problema della costruzione del MST sul grafo completo contenente tutte le sequenze in una serie di sottoproblemi.

Prendendo spunto da vari paper ed avendo in mente la struttura delle RDD, si è optato per la seguente sequenza di sottoproblemi da risolvere. 

\textbf{Primo gruppo - sottografi completi intrapartizione}

Si divide il grafo principale in $S$ sottografi disgiunti completi(se sullo stesso nodo vi sono punti naturalmente vicini fra di loro ciò velocizzerà moltissimo le fasi successive dell’algoritmo, ma non è necessario che lo siano). Per ognuno di questi sottografi si costruisce un MST. Questa è una serie di sottoproblemi che si presta molto bene ad essere risolta in distribuito, in quanto il calcolo del MST di ogni sottografo avviene in locale su ogni nodo. Inoltre, con una strategia di partizionamento efficace la procedura può essere ulteriormente velocizzata.

\textbf{Secondo gruppo - sottografi completi bipartiti}

Per ogni coppia di sottografi creati, si crea un grafo completo bipartito con gli elementi appartenenti alle coppie di sottografi. Quindi agli $S$ sottografi originali si aggiungeranno altri $\binom{S}{2}$ (numero di coppie senza ripetizione formabili da un insieme di $S$ elementi) sottografi completi bipartiti. Si vanno poi a costruire i MST su questi grafi completi bipartiti. 

Una volta che queste due categorie di sottoproblemi sono state risolte, si uniscono i MST e si tengono soltanto i collegamenti che hanno un peso (ovvero una distanza) minore da ogni nodo del grafo. In questo modo si ottiene un MST su tutto il grafo completo bipartito originale, che contiene al suo interno tutte le informazioni per la costruzione del dendrogramma.


\section{Fasi salienti e problemi riscontrati}

La struttura dei dati caricati quindi aveva la struttura di una coppia composta da un identificativo univoco per ogni sequenza e la sequenza vera e propria. In fase di trasformazione in RDD, si è deciso di associare ad ogni coppia una chiave che rappresentasse un blocco, ovvero una partizione della RDD (il numero di partizioni impostato al numero di \textit{core} presenti sulla macchina sulla quale si sta lavorando). Ciò avviene attraverso un partizionatore costruito \textit{ad hoc}. Quindi la prima trasformazione che si è fatta ha prodotto una JavaPairRDD dove ogni elemento aveva la struttura chiave-valore \texttt{[id partizione, (id sequenza, sequenza)]}. Questa sarà la struttura base con la quale si lavorerà in tutto il progetto. Il nome di tale JavaPairRDD è \texttt{dSeqPairs}.

\subsection{Sottografi completi intrapartizione}

In questo gruppo di sottoproblemi si trattava di calcolare i MST su partizioni locali. Una volta partizionata la RDD come spiegato in precedenza, 

Si è deciso di procedere con il metodo \texttt{mapPartitionsToPair}, in quanto tutti gli elementi appartenenti allo stesso sottografo si trovano sulla stessa partizione e all'interno dello stesso nodo. La classe \texttt{PrimMSTMap} è quella che si occupa di costrure il MST per i grafi completi. Tale classe usufruisce di oggetti creati su misura (con una classe apposita \texttt{Vertex}) e restituisce in output una PairRDD dove ogni elemento ha la struttura \texttt{[id sequenza 1, (id sequenza 2, distanza)]}.

L'idea generale della classe \texttt{PrimMSTMap} è quella di avere due contenitori di vertici, nella fattispecie uno chiamato \texttt{verteces} ed uno chiamato \texttt{graph}. L'algoritmo costruisce in modo iterativo i collegamenti più brevi possibili fra vertici prendendoli dal primo contenitore e \texttt{spostandoli} al secondo contenitore. Tale approccio ha permesso di ottimizzare l'utilizzo della memoria in questa fase.


\textbf{Descrizione dell'algoritmo}

In particolare, la classe \texttt{Vertex} contiene 5 (4 utilizzati in questa fase) campi:

\begin{itemize}
	\item \texttt{idSeq}: l'identificativo univoco della sequenza caricata
	\item \texttt{seq}: la sequenza associata all'identificativo
	\item \texttt{minDist}: la distanza che intercorre tra la sequenza con l'identificativo \texttt{idSeq} e la sequenza da cui parte la 					misurazione della distanza
	\item \texttt{precId}:  l'identificativo della sequenza dalla quale si sta 	misurando la distanza.
\end{itemize}

L'algoritmo ha le seguenti fasi:


\begin{enumerate}

	\item Inizializzazione: il primo vertice della lista \texttt{verteces} viene spostato in \texttt{graph}. Si va al passo 2.
	\item Se la lista \texttt{verteces} è vuota, andare al passo 4. Altrimenti, si computa la distanza fra ogni vertice rimanente in \texttt{verteces} e l'ultimo vertice aggiunto a \texttt{graph}. Ogniqualvolta si misura la distanza fra vertici, se essa risulta essere minore dell'attuale valore del campo \texttt{minDist}, si aggiornano i campi \texttt{minDist} con tale distanza e \texttt{precId} con il valore del campo \texttt{idSeq} del vertice in \texttt{graph}. Andare al passo 3.
	\item Si sposta in \texttt{graph} il vertice con valore del campo \texttt{minDist} minimo. Andare al passo 2.
	\item Finalizzazione.
	
\end{enumerate}



I problemi principali in questa fase sono stati 2:

\begin{enumerate}
	\item \texttt{ConcurrentModificationException}, che si verificava quando si cercava di aggiungere e/o togliere elementi da una lista durante un'iterazione su di essa.
	\item Problema di formato nell'output (produrre il collegamento fra vertici in formato (\texttt{precId} $\rightarrow$ \texttt{idSeq}) o viceversa?)
\end{enumerate}

Il primo problema è stato risolto adottando una classe denominata \texttt{ListIterator}, che permette di scorrere elementi in entrambe le direzioni ed allo stesso tempo permette modifiche della lista durante un'iterazione della stessa.

Per il secondo problema si è scelto di procedere con l'output \texttt{[idSeq, (precId, distanza)]}. Tale decisione è stata presa in via della consapevolezza della successiva fase di reduce. Produrre un output invertito (ovvero \texttt{[precID, (idSeq, distanza)]}) avrebbe comunque prodotto, nella fase di map, il MST. Tuttavia, nella fase di reduce, vi era il rischio altissimo di lasciar fuori dal MST complessivo dei nodi.

\subsection{Sottografi completi bipartiti}

La seconda serie di sottoproblemi si è rilevata molto più ostica della prima. Questa fase, oltre a rivelarsi pesante dal punto di vista computazionale di per sé, mostra in qualche modo i limiti applicativi di tale approccio. 

Vi era la necessità di mettere in relazione partizioni di \texttt{dSeqPairs} fra di loro a coppie, in modo da creare i sottografi completi bipartiti menzionati nella prima parte del presente documento. Ciò si è rivelato difficilmente attuabile, in quanto sarebbe stato necessario lavorare con una funzione di \texttt{cogroup} e poi iterare tra gli elementi dell'output prodotto. Si è scelta una strada alternativa, ovvero quella di scindere la RDD originale in tante RDD quante erano le partizioni della prima. Alle RDD risultanti si applica poi il metodo \texttt{union} a due a due, creando le $\binom{S}{2}$ combinazioni di RDD (nell'applicazione vengono salvata nella lista \texttt{RDDListCouple} e forzando ognuna di esse ad essere composta da un'unica partizione. Una volta fatto ciò, tali sottografi vengono riuniti sempre tramite unione in un'unica RDD. Il vantaggio di tale strategia è l'unione tra le RDD evita qualsiasi tipo di \textit{shuffle} ed inoltre la RDD finale sarà composta da $\binom{S}{2}$ partizioni contenenti esattamente le coppie di sottografi disgiunti (che possiamo chiamare sottopartizioni).

Ad esempio, avendo in principio una RDD con 4 partizioni (denominate da 0 a 3), la RDD risultante dai processi di trasformazione sopra descritti (denominata \texttt{dSeqBipartite}) conterrà $\binom{4}{2} = 6$ partizioni, i.e.

$$(0-1) \quad (0-2) \quad (0-3) \quad (1-2) \quad (1-3) \quad (2-3)$$

La classe che implementa l'algoritmo di Prim sui sottografi completi bipartiti non è molto diversa da quello per i sottografi completi, con la differenza che ad ogni iterazione i vertici di una sottopartizione puntano esclusivamente agli elementi dell'altra sottopartizione, e mai ad elementi della stessa sottopartizione. Ancora una volta, data la struttura della RDD sopra descritta, si è potuto utilizzare il metodo \texttt{MapPartitionsToPair}. 

La problematica principale in questo frangente è stata la gestione di una eccezione che si verificava qualora le due sottopartizioni di una partizione fossero di numerosità diversa - \texttt{NumberFormatException}. In tal caso, ci si può imbattere in un momento nel ciclo in cui si cerca di puntare gli elementi della prima sottopartizione agli elementi della seconda sottopartizione, ma quest'ultima è vuota. L'eccezione è gestita con un semplice blocco \textit{try-catch}.

\subsection{Fase di reduce}

Una volta che il MST è stato calcolato anche per i sottografi completi bipartiti, le RDD di output di questi ultimi e quella contenente i MST dei sottografi completi intrapartizione vengono uniti in una unica RDD attraverso una \texttt{reduceByKey} che semplicemente mantiene il collegamento di minima distanza per ogni nodo. La fase finale per la creazione del dendrogramma prevede esclusivamente che i collegamenti vengano ordinati in ordine ascendente secondo la distanza fra sequenze.

Sul dataset complessivo, i tempi di esecuzione sono stati di circa un'ora, più una decina di minuti per il caricamento delle relazioni su un dataset Neo4j. Evidentemente ciò che più ha pesato, oltre al fatto che è stato necessario calcolare comunque la distanza tra ogni coppia di sequenze (ma ciò è insito nella natura del problema - e le sequenze sono stringhe lunghe $\sim$ 6000 caratteri), è stata la necessità di dover riportare sullo stesso nodo copie delle partizioni originarie.  

\section{Conclusioni e considerazioni}

Alcune considerazioni finali vanno fatte su alcuni problemi riscontrati nell'output. Purtroppo ci si è resi conto, una volta caricati i collegamenti in Neo4j, che possono capitare dei cicli all'interno del grafo, che rendono il grafo complessivo non un MST. Ciò è dovuto ad una serie di fattori che riguardano l'uso congiunto dell'algoritmo da noi ideato e l'architettura di Spark. Per risolvere il problema, sono stati fatti più tentativi, fra cui quello di lavorare in un'ultima fase in sequenziale, senza tuttavia riscontrare successo. Forse utilizzando la API GraphX di Spark ci si potrebbe destreggiare meglio con questo problema. La soluzione resta comunque valida, ma restituisce un subottimo che tuttavia può essere limato fino a raggiungere il risultato previsto. 



 \renewcommand{\refname}{Riferimenti}
 
\begin{thebibliography}{1}

 \bibitem{HPC-Clust} J. F. M. Rodrigues \& Christian von Mering,  {\em HPC-CLUST: distributed hierarchical clustering for large sets of
nucleotide sequences}, Bioinformatics, Vol. 30 no. 2 2014, pg. 287–288
  
\bibitem{hendrix} \href{https://www.computer.org/csdl/proceedings/hipc/2012/2372/00/06507511.pdf}{William Hendrix et al., {\em Parallel Hierarchical Clustering on Shared Memory Platforms}, 2012 19th International Conference on High Performance Computing}

\bibitem{jin} \href{https://ieeexplore.ieee.org/document/7184911/}{Chen Jin et al., {\em A Scalable Hierarchical Clustering Algorithm Using Spark}, 2015 IEEE First Internation Conference on Big Data Computing Service and Applications}

\bibitem{guida} \href{http://eddylab.org/infernal/Userguide.pdf}{Guida in linea di Infernal}

\end{thebibliography}


\end{document}