package progetto;

/*
 * Questa classe � un semplice contatore che ci restituisce il numero di processori del computer
 * L'abbiamo utilizzata per scegliere il numero di partizioni
 */

public class Cores {
	
	public static int getCores() {
		
		int cores = Runtime.getRuntime().availableProcessors();
		
		return cores;
	}

}
