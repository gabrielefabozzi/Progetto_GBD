package progetto;
import java.util.ArrayList;
import java.util.List;

/*
 * La classe PartIndex contiene il metodo indexer, un metodo che noi abbiamo utilizzato per associare ad ogni sequenza un id di partizione
 * personalizzato. Cos� facendo, abbiamo potuto gestire le partizioni durante la fase in distribuito e ci� � stato necessario considerando il tipo
 * di approccio che abbiamo scelto
 */

public class PartIndex {
 
 public static List<String> indexer(List<String> lista){
  
  List<String> newList = new ArrayList<String>();
  
  int cores = Cores.getCores();  //il numero di partizioni ed i conseguenti indici dipendono dal numero di cores
  int batch = lista.size()/cores; 
  
  int remainder = lista.size()%cores;
  
  
  /*
   * Il successivo ciclo � quello che serve ad assegnare gli indici. Il ciclo � spezzato in due perch� nel caso in cui
   * batch non sia un numero intero dobbiamo assegnare l'ultimo indice a tutti i rimanenti elementi in lista
   */
  for(int i=0; i<=cores-1; i++) {
   int pos = i * batch;
   if(pos + batch <= lista.size()) {
    for(int j=pos; j<pos+batch; j++) {
     newList.add(lista.get(j).toString().concat("," + i));
    }

   }
  }
  for(int j=lista.size()-remainder; j<lista.size(); j++) {
      newList.add(lista.get(j).toString().concat("," + (cores-1)));
    }
  return newList;
  }
 
  
 }