package progetto;

/*
 * IMPORTANTE: Questa classe non � stata utilizzata. E' stato un tentativo di gestire l'ultima parte del problema in sequenziale, tentativo che deriva
 * dalle difficolt� dell'approccio in distribuito. Data la complessit� del codice (elevata in quanto sono presenti diverse catene di nested for/if) e la sua
 * non efficienza � stata eliminata dal progetto, ma mantenuta per mostrare i tentativi effettuati.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import scala.Tuple2;

public class MST {
	
	
	public static boolean ifContains(List<Tuple2<Integer, Tuple2<Integer, Integer>>> lista, int i) {
		int counter = 0;
		for(Tuple2<Integer, Tuple2<Integer, Integer>> t : lista) {
			if(t._2._1==i) {
				counter++;
			}
		}
		if(counter>0){
			return true;
		}else {
			return false;
		}
	}
	
	public static List<Tuple2<Integer, Tuple2<Integer, Integer>>> getMST(List<Tuple2<Integer, Tuple2<Integer, Integer>>> lista){
		
		//Creo le liste che conterranno i miei oggetti
		List<Tuple2<Integer, Tuple2<Integer, Integer>>> tempList = new ArrayList<Tuple2<Integer, Tuple2<Integer, Integer>>>();
		List<Tuple2<Integer, Tuple2<Integer, Integer>>> output = new ArrayList<Tuple2<Integer, Tuple2<Integer, Integer>>>();
		
		//creo l'oggetto minimo
		Tuple2<Integer, Tuple2<Integer, Integer>> minimum = new Tuple2<Integer, Tuple2<Integer, Integer>>(-1, new Tuple2<Integer, Integer>(0,0));
		
		
		for(int i=0; i<lista.size(); i++) {
			
			//Aggiungo gli elementi che devono essere confrontati alla lista temporanea
			for(int j=0; j<lista.size(); j++) {
				if(lista.get(j)._1==i) {
					tempList.add(lista.get(j));
				}else {
					continue;
				}
			}
			
			//confronto gli elementi della lista
			if(tempList.size()==1) {
				
				output.add(tempList.get(0)); //se la lista � composta da un solo elemento, sar� chiaramente quella la relazione minima
				
			}else{  //altrimenti ho bisogno di un codice un po' pi� complesso
				for(int t1=0; t1<tempList.size(); t1++){
					for(int t2=0; t2<tempList.size(); t2++){
						if(t2<=t1) {
							Tuple2<Integer, Tuple2<Integer, Integer>> token1 = lista.get(t1);
							Tuple2<Integer, Tuple2<Integer, Integer>> token2 = lista.get(t2);
							if(token1._2._2<token2._2._2 & (output.isEmpty()==true | ifContains(output, token1._2._1)==false)){
								minimum = token1;
							}else if(token2._2._2<token1._2._2 & (output.isEmpty()==true | ifContains(output, token1._2._1)==false)){
								minimum = token2;
							}
						}else{
							continue;
						}
					}
				}
				
				output.add(minimum);
			}
			//In questo modo ho aggiunto alla lista in output tutte le relazioni evitando sia le ridondanze sia la perdita di informazione
		}
		
		return output;
	}

}
