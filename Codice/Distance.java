package progetto;

/*
 * La classe Distance calcola la distanza fra due stringhe di testo ed � basta sul concetto di Distanza di Hamming, ovviamente personalizzato
 * secondo le nostre esigenze. Di seguito, una legenda dei punteggi associati ad ogni disuguaglianza
 */

public class Distance {
	
	public static int computeDistance(String arg0, String arg1) throws LunghezzeDiverseException{
		
		/*Legenda della metrica:
		 * A A = 0
		 * . . = 0
		 * - * = 1
		 * . A = 2
		 * Mismatch = 2
		 */
		if(arg0.length() != arg1.length()) {
			throw new LunghezzeDiverseException("Le due sequenze sono di lunghezza diversa.");
		}else {
		int distance = 0;
		arg0 = arg0.toLowerCase();
		arg1 = arg1.toLowerCase();
		for(int i=0; i<arg0.length(); i++) {
			char a = arg0.charAt(i);
			char b = arg1.charAt(i);
			if(a==b && a != '-') {
				distance += 0;
			}else if(a==b && a == '-') {
				distance += 1;
			}else if(a != b && ((a != '-' & b == '-')||(a == '-' & b != '-'))) {
				distance += 1;
			}else {
				distance +=2;
			}
		}
		return distance;
		}

	}
}
