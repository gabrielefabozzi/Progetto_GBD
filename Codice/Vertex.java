package progetto;

public class Vertex {

	/*
	 * L'oggetto vertice che verr� usato per il calcolo del MST ha quattro elementi. 
	 * - Un id univoco di sequenza
	 * - La sequenza associata
	 * - La minima distanza (la distanza dall'oggetto vertice pi� vicino dentro al grafo)
	 * - L'id del nodo con il quale la minima distanza � calcolata
	 * 
	 */
	
	private int idSeq;
	private String seq;
	private int minDist;
	private int precId = 0;
	private String idPart;
	
	public int getIdSeq() {
		return idSeq;
	}
	public void setIdSeq(int idSeq) {
		this.idSeq = idSeq;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public int getMinDist() {
		return minDist;
	}
	public void setMinDist(int minDist) {
		this.minDist = minDist;
	}
	public int getPrecId() {
		return precId;
	}
	public void setPrecId(int precId) {
		this.precId = precId;
	}
	public String getIdPart() {
		return idPart;
	}
	public void setIdPart(String idPart) {
		this.idPart = idPart;
	}
	
	
	//costruttore
	public Vertex(int idSeq, String seq, int minDist, String idPart) {
		super();
		this.idSeq = idSeq;
		this.seq = seq;
		this.minDist = minDist;
		this.idPart = idPart;
	}
	
	

	
}
