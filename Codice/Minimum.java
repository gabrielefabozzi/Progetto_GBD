package progetto;

/*
 * Semplice classe usata in fase di reduce per restituire il minimo delle sequenze
 */

import org.apache.spark.api.java.function.Function2;

import scala.Tuple2;

public class Minimum implements Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> {

	@Override
	public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> arg0, Tuple2<Integer, Integer> arg1)
			throws Exception {
		if(arg0._2 <= arg1._2) {
			return arg0;
		}else {
			return arg1;
		}
	}

}
