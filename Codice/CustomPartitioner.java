package progetto;
import org.apache.spark.Partitioner;
import java.util.Random;
 

/*
 * Questa classe contiene il partizionatore personalizzato che abbiamo utilizzato con il metodo partitionBy per dividere il dataset
 * originale in pi� partizioni sulle quali applicare il mapPartitionsToPair
 */

class CustomPartitioner extends Partitioner{
 
	private int numParts;
	
	public CustomPartitioner(int i) {
		numParts=i;
	}
 
	@Override
	public int numPartitions()
	{
	    return numParts;
	}
 
	public int getPartition(Object key){

		String chiave = key.toString();
		String partition = chiave.split(",")[0];  //Questo � il criterio di partizione. Infatti, l'oggetto in posizione zero � l'indice di partizione da noi assegnato
		int partizione = Integer.parseInt(partition);
		return partizione;
	
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof CustomPartitioner)
		{
			CustomPartitioner partitionerObject = (CustomPartitioner)obj;
			if(partitionerObject.numParts == this.numParts)
				return true;
		}
	
		return false;
	   }

}