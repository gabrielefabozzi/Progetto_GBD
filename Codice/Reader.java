package progetto;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import au.com.bytecode.opencsv.CSVWriter;
import scala.Tuple2;


/*
 * La classe Reader � la classe che abbiamo utilizzato per leggere le sequenze. E' dotata anche di un metodo che permette, utilizzando un boolean,
 * non solo di leggere la sequenza ma anche di stampare il csv
 */
public class Reader{
		
		public String in;
		public String out;
		public String delimiter;
		public boolean print;
		public int index = 0;
		
		//Costruttore
		public Reader(String in, String out, String delimiter) {
			super();
			this.in = in;
			this.out = out;
			this.delimiter = delimiter;
			this.print = print;
		}
		
		/*
		 * Classe di lettura
		 */
		
		public List<String> lettore(Reader r, int a, int o) throws IOException {
	        List<String> file = new ArrayList<String>(Files.readAllLines(Paths.get(r.in)));
	        List<String> seq = new ArrayList<String>();
	        
	        
	        for(int i=0; i < file.size(); i=i+o) {
	        	String s = file.get(i);
	        	String[] tokens = s.split(r.delimiter);
	        	String str = new String(index + "," + tokens[a]);
	        	seq.add(i/o, str);
	        	index++;
	        	}

        	return seq;
	        
	        
	        
		}
		
		//Metodo per la stampa di un csv
		public void stampaCSV(Reader r, int a, int o) throws IOException {
			List<String> file = new ArrayList<String>(Files.readAllLines(Paths.get(r.in)));
	        List<String[]> seq = new ArrayList<String[]>();
	        
	        
	        for(int i=0; i < file.size(); i=i+o) {
	        	String s = file.get(i);
	        	String[] tokens = s.split(r.delimiter);
	        	String[] str = new String[]{Integer.toString(i),tokens[a]};
	        	seq.add(i/o, str);
	        	}
	        String csv = r.out;
        	CSVWriter writer = new CSVWriter(new FileWriter(csv));
        	writer.writeAll(seq);
        	writer.close();
		}
}