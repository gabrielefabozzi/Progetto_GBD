package progetto;

import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

		/*
		 * Tale classe ha come unico obiettivo quello di prendere l'RDD di partenza e spezzarlo in pi� RDD.
		 */

public class Separatore implements Function<Tuple2<String, Tuple2<Integer, String>>, Boolean> {

	int partizione;
	public Separatore(int partizione) {
		this.partizione = partizione;
	}
	
	public Boolean call(Tuple2<String, Tuple2<Integer, String>> arg0) throws Exception {
		
		
		/*
		 * Quello che stiamo facendo qui � controllare gli elementi per poterli partizionare
		 * A seconda dell'indice il metodo Separator � in grado di mappare l'RDD e crearne una nuova con quello specifico indice
		 */
		if(Integer.parseInt(arg0._1) == partizione) {
			return true;
		}
		return false;
	}


}

