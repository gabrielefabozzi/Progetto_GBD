package progetto;


/*
 * Eccezione che serve a gestire l'eventualitą di due stringhe di lunghezza diversa
 */
public class LunghezzeDiverseException extends Exception{
	
	private String errore;
	
	

	public String getErrore() {
		return errore;
	}



	public void setErrore(String errore) {
		this.errore = errore;
	}



	public LunghezzeDiverseException(String errore) {
		this.errore=errore;
	}
}
