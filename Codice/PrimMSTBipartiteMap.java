package progetto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/*
 * Questa classe � stata utilizzata per calcolare gli MST fra coppie di partizioni utilizzando l'algoritmo di Prim.
 * E' simile alla classe usata per i grafi completi, ovviamente vi sono per� degli accorgimenti che ci permettono di adattarci al tipo
 * di struttura che abbiamo in questo caso
 */

import org.apache.spark.api.java.function.PairFlatMapFunction;

import scala.Tuple2;

public class PrimMSTBipartiteMap implements PairFlatMapFunction<Iterator<Tuple2<String,Tuple2<Integer, String>>>, Integer, Tuple2<Integer,Integer>> {

	
	public Iterator<Tuple2<Integer, Tuple2<Integer, Integer>>> call(
			Iterator<Tuple2<String, Tuple2<Integer, String>>> arg0) throws Exception {
		
		//creo le liste vuote di vertici, grafo e output
		List<Vertex> verteces = new ArrayList<Vertex>();
		List<Vertex> graph = new ArrayList<Vertex>();
		List<Tuple2<Integer, Tuple2<Integer, Integer>>> output = new ArrayList<Tuple2<Integer, Tuple2<Integer, Integer>>>();
		
		//Inizio l'algoritmo
		//Popolo la lista di vertici
		
		for(Iterator<Tuple2<String, Tuple2<Integer, String>>> it = arg0; it.hasNext();) {
			String[] tokens = it.next().toString().split("[(|,|)]+");
			Vertex v = new Vertex(Integer.parseInt(tokens[2]), tokens[3], Integer.MAX_VALUE, tokens[1]);
			verteces.add(v);
		}
		
		//inizializzo il primo vertice
		Vertex starting = verteces.get(0);
		starting.setMinDist(0);
		graph.add(starting);
		verteces.remove(starting);
		ListIterator<Vertex> graphIt = graph.listIterator();
		
		/*
		 * Il successivo ciclo funziona esattamente come quello per i grafi completi, tenendo conto delle dovute
		 * accortezze.
		 */
		try {
		while(graphIt.hasNext()) {
			
			Vertex currentGraph = graphIt.next();
			Vertex nearestV = new Vertex(-1, null, Integer.MAX_VALUE, null);		//creo un vertice vuoto che conterr� il vertice pi� vicino
			
			if(verteces.isEmpty() == true) {		//se verteces � vuoto il ciclo si ferma
				break;
			}
			
			
			for(Vertex v : verteces) {		//ciclo su tutti gli elementi rimasti in verteces
				if(Integer.parseInt(currentGraph.getIdPart()) != Integer.parseInt(v.getIdPart())) {  //tranne quelli con idPart uguale
					int distanza = Distance.computeDistance(currentGraph.getSeq(), v.getSeq());
					if(distanza<= v.getMinDist()){		//se la distanza � inferiore alla corrente di w, aggiorno minDist e precId
					v.setMinDist(distanza);
					v.setPrecId(currentGraph.getIdSeq());
					}
					if(v.getMinDist()<=nearestV.getMinDist()) {		//tengo conto del vertice pi� vicino
					nearestV = v;
					}
				}else{
					continue;
				}
			}
			
			/*
			 * Estrapoliamo l'indice del vertice nearestV in modo da poterlo richiamare nella condizione del removeIf
			 */
			
			int nvId = nearestV.getIdSeq();
			
			//aggiungo nearestV al grafo e lo rimuovo da verteces
			
			/*
			 *  Aggiungiamo il vertice pi� vicino all'iteratore graphIt. Per fare ci�, utilizziamo il comando add che aggiunge l'oggetto nearestV.
			 *  Ora, poich� il comando add aggiunge il nostro oggetto non in coda ma in testa alla lista, per puntarlo dobbiamo richiamare il previous
			 *  In questo modo, l'oggetto nearestV sar� quello che useremo per i prossimi confronti, seguendo lo schema di Prim.
			 */
			
			graphIt.add(nearestV);
			graphIt.previous();

			verteces.removeIf(e -> e.getIdSeq() == nvId);
			
			//aggiungiamo l'elemento appena inserito ad output
			if(nearestV.getIdSeq() == -1) {
				continue;
			};
			
			output.add(new Tuple2<Integer, Tuple2<Integer,Integer>>(nearestV.getIdSeq(), new Tuple2<Integer,Integer>(nearestV.getPrecId(),nearestV.getMinDist())));
			/*
			 * Notare che il nodo di partenza � in realt� quello a cui � arrivato il collegamento, e questo perch� una schematizzazione
			 * del genere ci permette effettivamente in fase di reduce di essere sicuri di non escludere alcun nodo 
			 */
			}
		}catch(NumberFormatException ex) {
			for(Vertex v : verteces){
				output.add(new Tuple2<Integer, Tuple2<Integer,Integer>>(v.getIdSeq(), new Tuple2<Integer,Integer>(v.getPrecId(),v.getMinDist())));
			}
		}
		
		/*
		 * Il codice sottostante serve a far s� che si crei il collegamento minimo anche per l'elemento 0, che altrimenti (essendo in prima posizione per ogni
		 * coppia di algoritmi, non sarebbe utilizzato in fase di reduce.
		 */
		Tuple2<Integer, Tuple2<Integer, Integer>> token = output.get(0);
		Tuple2<Integer, Tuple2<Integer, Integer>> token1 = new Tuple2<Integer, Tuple2<Integer, Integer>>(token._2._1, new Tuple2<Integer, Integer>(token._1, token._2._2));
		output.remove(token);
		output.add(token1);
		return output.iterator();
	}

}

