package progetto;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.JavaPairRDD;



import scala.Tuple2;

public class HCA {

	public static void main(String[] args) throws IOException, SQLException {
	
		/*
		 * Definiamo una variabile temporale che ci permetter� di tenere conto dei tempi di esecuzione
		 */
		
		long startTime = System.nanoTime();
		
		/*
		 * Le seguenti due righe di codice permettono, specificando la cartella di input, quella di output ed un separatore, di 
		 * prendere il file di input con i nomi delle specie che si stanno considerando e trasformarlo in formato .csv. Si crea
		 * un oggetto di classe Reader da cui poi si richiama un metodo che permette di stampare a .csv. Il metodo � commentato poich�
		 * ovviamente pu� funzionare solo indicando le corrette directories
		 */
		 //Reader r = new Reader("C:\\Users\\ricci\\Desktop\\Universit�\\Gestione_Big_Data\\Progetto\\Dati - natura, lettura ecc\\lista_mini.txt", "C:\\Users\\ricci\\Desktop\\Universit�\\Gestione_Big_Data\\Progetto\\Dati - natura, lettura ecc\\listaSpecie_mini.csv", "#=GS | DE |strain ");
		 //r.stampaCSV(r,2,1);
		 
		
		/*
		 * Le seguenti due righe permettono, specificando un percorso di input ed un separatore, di acquisire le sequenze che
		 * verranno processate in parallelo successivamente. Una volta lette con Reader, si crea una lista in cui si prepara in 
		 * anticipo la struttura distribuita.
		 * 
		 * Gli oggetti all'interno della lista seq sono delle stringhe. Essi sono costruite con un metodo
		 * della classe PartIndex chiamato indexer, descritto pi� in dettaglio nella sua classe.
		 * 
		 * Una volta passate attraverso lettore e indexer, le nostre stringhe avranno tale struttura: ID sequenza, sequenza, id partizione
		 */
		
		Reader sequenze = new Reader("C:\\Users\\ricci\\Desktop\\Universit�\\Gestione_Big_Data\\Progetto\\Dati - natura, lettura ecc\\lista_mini_mini.txt","null","[ ]+");
		List<String> seq = PartIndex.indexer(sequenze.lettore(sequenze, 1, 2));
		
		
		/*
		 * Impostazioni generali
		 */
		
		
		Logger.getLogger("org").setLevel(Level.ERROR);
		Logger.getLogger("akka").setLevel(Level.ERROR);
		SparkConf sc = new SparkConf();
		sc.setAppName("HCA");
		sc.setMaster("local[*]");
		JavaSparkContext jsc = new JavaSparkContext(sc);
		
		/*
		 * Si crea innanzitutto una RDD con l'id univoco della sequenza, la sequenza, ed il futuro id di partizione.
		 */
		
		JavaRDD<String> dSeq = jsc.parallelize(seq);
		
		/*
		 * Si trasforma la RDD in una PairRDD che ha come chiave l'id della partizione a cui apparterr� ogni entrata
		 * e come valore la tupla (id sequenza, sequenza).
		 */
		
		
		JavaPairRDD<String,Tuple2<Integer, String>> dSeqPairs = dSeq.flatMapToPair(
				new PairFlatMapFunction<String, String, Tuple2<Integer, String>>() {

					@Override
					public Iterator<Tuple2<String,Tuple2<Integer, String>>> call(
							String arg0) throws Exception {
						
						List<Tuple2<String,Tuple2<Integer, String>>> output = new ArrayList<Tuple2<String,Tuple2<Integer, String>>>();
						
						output.add(new Tuple2<String,Tuple2<Integer, String>>(arg0.split(",")[2], new Tuple2<Integer, String>(Integer.parseInt(arg0.split(",")[0]), arg0.split(",")[1])));
						
						return output.iterator();
					}
			});
				
		
		/*
		 * FASE1: MST SU GRAFI COMPLETI
		 */
		
		/*
		 * Il metodo "partitionBy" permette di partizionare la PairRDD con la quale si lavora secondo un partizionatore personalizzato.
		 * La ratio di tale scelta sta nel fatto che l'output di Infernal (programma allineatore di sequenze) elenca sequenze simili 
		 * l'una accanto all'altro, e si vuole quindi che le partizioni siano in blocco e, per scelta, una per ogni nodo a disposizione.
		 */
		
		dSeqPairs = dSeqPairs.partitionBy(new CustomPartitioner(Cores.getCores()));
		
		/*
		 * Il calcolo degli MST sui grafi completi si risolve in un map, dove attraverso il metodo mapPartitionsToPair si riesce
		 * a svolgere l'operazione in locale su ogni partizione. Il boolean in mapPartitionsToPair � un'opzione che permette di
		 * forzare la persistenza del partizionamento.
		 */
		
		JavaPairRDD<Integer, Tuple2<Integer,Integer>> dSeqComplete = dSeqPairs.mapPartitionsToPair(new PrimMSTMap(), true);
		
		/*
		 * 
		 * FASE 2: CALCOLO MST SU GRAFI COMPLETI BIPARTITI
		 */
		
		/*
		 * Il grosso problema qui � l'impossibilit� di puntare a specifiche partizioni all'interno della singola RDD.
		 * La soluzione seguente consiste nello spacchettare la PairRDD di partenza in tante RDD quante sono le partizioni
		 * di partenza. 
		 */
		
		List<JavaPairRDD<String,Tuple2<Integer, String>>> RDDList = new ArrayList<JavaPairRDD<String,Tuple2<Integer, String>>>();
		

		for(int i = 0; i < Cores.getCores(); i++) {
			RDDList.add(dSeqPairs.filter(new Separatore(i)));
		}
		
		/*
		 * Dopo aver formato le singole RDD, registriamo le coppie che devono essere confrontate in una lista. Coalesce serve a 
		 * far s� che la coppia di RDD venga considerata come una sola partizione (questo ci sar� utile nella fase di map)
		 * 
		 */
		
		List<JavaPairRDD<String,Tuple2<Integer, String>>> RDDListCouple = new ArrayList<JavaPairRDD<String,Tuple2<Integer, String>>>();
		
		for(JavaPairRDD i : RDDList) {
			for(JavaPairRDD j : RDDList) {
				if(i.id()>=j.id()) {
					continue;
				}
				RDDListCouple.add(i.union(j).coalesce(1, false));
				
			}
		}
		
		JavaPairRDD dSeqUnion = RDDListCouple.get(0);
		
		RDDListCouple.remove(0);
		
		for(JavaPairRDD i : RDDListCouple) {
			
			dSeqUnion = dSeqUnion.union(i);
		}
		
		/*
		 * Abbiamo provato con JavaSparkContext.union, ma purtroppo agglomerava partizioni. Ci� ci ha obbligato ad usare 
		 * un'unione iterativa, che ci ha permesso di mantenere le coppie di RDD come partizioni separate.
		 */
		
		/*
		 * Il map viene eseguito sulle diverse partizioni, per sfruttare al meglio il lavoro in parallelo
		 */
		JavaPairRDD<Integer, Tuple2<Integer,Integer>> dSeqBipartite = dSeqUnion.mapPartitionsToPair(new PrimMSTBipartiteMap(), true);
		
		/*
		 * Uniamo infine i due RDD, per ottenere quello finale su cui faremo il lavoro in sequenziale
		 */
		
		JavaPairRDD dSeqFinal = dSeqComplete.union(dSeqBipartite);
		
		
		/*
		*Rimane ora solo da fare la reduce sui dati uniti
		*/
		
		JavaPairRDD dSeqMST = dSeqFinal.reduceByKey(new Minimum());
		
		List<Tuple2<Integer, Tuple2<Integer, Integer>>> linkList = dSeqMST.collect();
		
		
		jsc.close();
		
		/*
		 * Non ci resta che ricostruire il grafo su Neo4j
		 */
		
		List<Tuple2<Integer, Tuple2<Integer, Integer>>> finalList = MST.getMST(linkList);
		
		boolean debug = true;
		
		if(debug) {
		Connection c = DriverManager.getConnection("jdbc:neo4j:bolt://localhost", "neo4j", "banana");
		Statement st = c.createStatement();
		
		for(Tuple2<Integer, Tuple2<Integer, Integer>> t : linkList) {
			
			int origine = t._2._1;
			int arrivo = t._1;
			
			System.out.println("Creazione arco " + origine + " -> " + arrivo);
			
			String cql = "MATCH (n),(m) WHERE n.idSpecie = "+ origine +" AND m.idSpecie = "+ arrivo +" create (n)-[:Edge]->(m)";
			st.executeUpdate(cql);
		}
		
		
		c.close();
		}
		long endTime = System.nanoTime();
		long runningTime = endTime - startTime;
		System.out.println("Execution time is: " + runningTime/1000000000);
		
		/*
		 * Chiudiamo il contesto spark
		 */
		
		
	}
	
	
}
